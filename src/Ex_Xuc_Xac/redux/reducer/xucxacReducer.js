import { CHANGE_PLAY, TAI_XIU } from "../constant";

let initialState = {
  mangXucXac: [
    { ma: 1, img: "./imgXucSac/1.png" },
    { ma: 1, img: "./imgXucSac/1.png" },
    { ma: 1, img: "./imgXucSac/1.png" },
  ],
  choose: "",
  tongdiem: 3,
  sobanthang: 0,
  sobanchoi: 0,
};
export const xucxacReducer = (state = initialState, action) => {
  switch (action.type) {
    case TAI_XIU: {
      action.value ? (state.choose = "TÀI") : (state.choose = "XỈU");
      return { ...state };
    }
    case CHANGE_PLAY: {
      if (state.choose == "") {
        alert("vui lòng chọn Tài hoặc Xỉu");
        return;
      }
      let newArray = [...state.mangXucXac];
      newArray.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return (item.img = `./imgXucSac/${random}.png`), (item.ma = random);
      });
      //tổng điểm
      let sum = 0;
      state.mangXucXac.map((item) => {
        return (sum += item.ma);
      });
      //số bàn thắng tài >11 , xỉu < 11
      if (
        (state.choose == "TÀI" && sum > 11) ||
        (state.choose == "XỈU" && sum < 11)
      ) {
        state.sobanthang++;
      }
      //số bàn chơi
      state.sobanchoi++;
      return { ...state, mangXucXac: newArray, tongdiem: sum };
    }
    default:
      return { ...state };
  }
};
