import React, { Component } from "react";
import TaiXiu from "./TaiXiu";
import ThongTin from "./ThongTin";
import "./xucxac.css";
export default class Ex_Xuc_Xac extends Component {
  render() {
    return (
      <div
        className="Game"
        style={{ background: "url(./imgXucSac/bgGame.png)", height: "100vh" }}
      >
        <TaiXiu />
        <ThongTin />
      </div>
    );
  }
}
