import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_PLAY } from "./redux/constant";

class ThongTin extends Component {
  render() {
    return (
      <div className="display-4">
        <button
          onClick={() => {
            this.props.handleChangePlay();
          }}
          className="btn btn-info p-4"
        >
          Play Game
        </button>
        <p>
          Bạn chọn: <span className="text-danger">{this.props.choose}</span>
        </p>
        <p>Tổng Điểm: {this.props.tongdiem}</p>
        <p>Số Bàn Thắng: {this.props.sobanthang}</p>
        <p>Số bàn chơi: {this.props.sobanchoi}</p>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    choose: state.xucxacReducer.choose,
    tongdiem: state.xucxacReducer.tongdiem,
    sobanthang: state.xucxacReducer.sobanthang,
    sobanchoi: state.xucxacReducer.sobanchoi,
  };
};
let mapDispatchToDrops = (dispatch) => {
  return {
    handleChangePlay: () => {
      dispatch({
        type: CHANGE_PLAY,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToDrops)(ThongTin);
