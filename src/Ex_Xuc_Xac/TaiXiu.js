import React, { Component } from "react";
import { connect } from "react-redux";
import { TAI_XIU } from "./redux/constant";

class TaiXiu extends Component {
  state = {
    active: null,
  };
  handleChangeActive = (value) => {
    this.setState({
      active: value,
    });
  };
  render() {
    return (
      <div className="row container m-auto pt-5">
        <button
          style={{
            transform: `scale(${this.state.active == "TAI" ? 2 : 1})`,
          }}
          onClick={() => {
            this.props.handleChangTaiXiu(true);
            this.handleChangeActive("TAI");
          }}
          className="btn btn-success p-5 col-2"
        >
          TÀI
        </button>
        <div className="col-8">
          <img src={this.props.mangXucXac[0].img} width="50" alt="" />
          <img src={this.props.mangXucXac[1].img} width="50" alt="" />
          <img src={this.props.mangXucXac[2].img} width="50" alt="" />
        </div>
        <button
          style={{
            transform: `scale(${this.state.active == "XIU" ? 2 : 1})`,
          }}
          onClick={() => {
            this.props.handleChangTaiXiu(false);
            this.handleChangeActive("XIU");
          }}
          className="btn btn-success p-5 col-2"
        >
          XỈU
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucxacReducer.mangXucXac,
  };
};
let mapDispatchToDrops = (dispatch) => {
  return {
    handleChangTaiXiu: (value) => {
      dispatch({ type: TAI_XIU, value: value });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToDrops)(TaiXiu);
