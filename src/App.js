import logo from "./logo.svg";
import "./App.css";
import Ex_Xuc_Xac from "./Ex_Xuc_Xac/Ex_Xuc_Xac";

function App() {
  return (
    <div className="App">
      <Ex_Xuc_Xac />
    </div>
  );
}

export default App;
