import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { rootReducer_Ex_Xuc_Xac } from "./Ex_Xuc_Xac/redux/reducer/rootReducer";
const root = ReactDOM.createRoot(document.getElementById("root"));
const store = createStore(rootReducer_Ex_Xuc_Xac);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
